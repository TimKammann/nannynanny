Rails.application.routes.draw do

  root                'static_pages#home'
  get    'help'    => 'static_pages#help'
  get    'about'   => 'static_pages#about'
  get    'contact' => 'static_pages#contact'
  get    'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'


  resources :parents

  resources :jobs

  resources :conversations do
    resources :messages
  end

  resources :users do
    member do
      get :ratings
    end
  end

  resources :requests,       only: [:create, :destroy]

  resources :ratings

  resources :relationships,       only: [:create, :destroy]

  get    'search'   => 'parents#search'

  get    'calculators'   => 'calculators#new'
  post   'calculators'  => 'calculators#create'

  post 'parents/optimize', :to => 'parents#optimize'

  patch 'requests/accept', :to => 'requests#accept'
  patch 'requests/pay', :to => 'requests#pay'

  post 'relationships/create', :to => 'relationships#create'
  patch 'relationships/destroy', :to => 'relationships#destroy'

  post 'sortings/by_created_at', :to => 'sortings#by_created_at'
  post 'sortings/by_matching', :to => 'sortings#by_matching'
  post 'sortings/by_rate', :to => 'sortings#by_rate'

  post 'matchings/match', :to => 'matchings#match'

  get    'proto_premium'   => 'static_pages#proto_premium'
  post   'static_pages/premium'   => 'static_pages#premium'
  get    'proto_pay'   => 'static_pages#proto_pay'

  get    'index'   => 'matchings#index'

end
