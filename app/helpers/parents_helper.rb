module ParentsHelper

  def parent?
    !current_user.parent.nil?
  end

end
