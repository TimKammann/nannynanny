module JobsHelper

  def job?
    !current_user.job.nil?
  end
end
