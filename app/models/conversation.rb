class Conversation < ActiveRecord::Base

  belongs_to :job_user, :foreign_key => :job_user_id, class_name: "User"
  belongs_to :parent_user, :foreign_key => :parent_user_id, class_name: "User"
  has_many :messages, dependent: :destroy
  validates_uniqueness_of :job_user_id, :scope => :parent_user_id

  scope :between, -> (parent_user_id, job_user_id) do
    where("(conversations.parent_user_id = ? AND conversations.job_user_id =?)
    OR (conversations.parent_user_id = ? AND conversations.job_user_id =?)", parent_user_id, job_user_id, job_user_id, parent_user_id)
    end
end
