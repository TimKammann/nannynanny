class Job < ActiveRecord::Base
  belongs_to :user

  validates :user_id, presence: true
  validates :city, presence: true, length: { maximum: 120 }
  validates :content, presence: true, length: { maximum: 800 }
  validates :valid_from, presence: true
  validates :valid_until, presence: true
  validates :time, presence: true
  validates :kids, presence: true
  validates :nanny_gender, presence: true
  validates :gender, presence: true
  validates :experience, presence: true
  validates :rate, presence: true
  validates :age, numericality: { greater_than_or_equal_to: 15, :message => "muss gesetzt sein." }
  validate :age_of_kids

  has_many :passive_requests, class_name:  "Request",
           foreign_key: "potential_id",
           dependent:   :destroy
  has_many :interesteds, through: :passive_requests, source: :interested

  def job_time
    created_at.strftime("%d.%m.%y")
  end

  def age_of_kids
    errors.add(:base, "Bitte Alter des Kinders/der Kinder angeben") unless baby || toddler || preschooler || gradeschooler || teenager
  end
end
