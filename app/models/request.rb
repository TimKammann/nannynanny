class Request < ActiveRecord::Base
  belongs_to :interested, class_name: "Parent"
  belongs_to :potential, class_name: "Job"

  default_scope -> { order(created_at: :desc) }

  validates :interested_id, presence: true
  validates :potential_id, presence: true
end

