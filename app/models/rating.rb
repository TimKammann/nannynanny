class Rating < ActiveRecord::Base
  belongs_to :user
  validates :content, length: { maximum: 360 }
  validates :sender_id, presence: true
  validates :receiver_id, presence: true
end
