class Message < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user

  validates :content, presence: true, length: { maximum: 600 }
  validates_presence_of :conversation_id, :user_id

end
