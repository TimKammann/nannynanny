class Parent < ActiveRecord::Base
  belongs_to :user

  has_many :active_requests, class_name:  "Request",
           foreign_key: "interested_id",
           dependent:   :destroy
  has_many :potentials, through: :active_requests, source: :potential
  has_one :sorting, dependent: :destroy

  validates :user_id, presence: true
  validates :city, presence: true, length: { maximum: 120 }
  validates :content, length: { maximum: 800 }
  validates :valid_from, presence: true
  validates :valid_until, presence: true
  validates :time, presence: true
  validates :kids, presence: true
  validates :nanny_gender, presence: true
  validates :gender, presence: true
  validates :lower_age, presence: true
  validates :upper_age, presence: true
  validates :experience, presence: true
  validate :age_of_kids

  def age_of_kids
    errors.add(:base, "Bitte Alter des Kinders/der Kinder angeben") unless baby || toddler || preschooler || gradeschooler || teenager
  end

end

