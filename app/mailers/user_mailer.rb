class UserMailer < ApplicationMailer
  default from: 'info@babyritter.com'

  def welcome_email(user)
    @user = user
    @url  = 'http://babyritter.com/login'
    mail(to: @user.email, subject: 'Willkommen bei BabyRitter')
  end

  def parent_request_email(user)
    @user = user
    @url = 'http://babyritter.com/show'
    mail(to: @user.email, subject: 'Sie haben eine Anfrage erhalten')
  end
end

