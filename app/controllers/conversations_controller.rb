class ConversationsController < ApplicationController
before_action :logged_in_user

  def index
    nanny_users = Job.pluck(:user_id)
    @users = User.where(id: nanny_users)
    @conversations = Conversation.all
    @all_the_users = User.all
  end

  def create
    if Conversation.between(params[:parent_user_id], params[:job_user_id]).present?
      @conversation = Conversation.between(params[:parent_user_id],
                                           params[:job_user_id]).first
    else
      @conversation = Conversation.create!(conversation_params)
    end
    redirect_to conversation_messages_path(@conversation)
  end

  def destroy
    Conversation.find(params[:id]).destroy
    flash[:success] = "Chatverlauf wurde gelöscht"
    redirect_to conversations_path
  end


  private

  def conversation_params
    params.permit(:parent_user_id, :job_user_id)
  end

end
