class SortingsController < ApplicationController

  def by_created_at
    Sorting.find_by(parent_id: current_user.parent.id).update_columns(:by_created_at => true,
                                                                       :by_matching => false,
                                                                       :by_rate => false)
    redirect_to search_path
  end

  def by_rate
      Sorting.find_by(parent_id: current_user.parent.id).update_columns(:by_created_at => false,
                                                                        :by_matching => false,
                                                                        :by_rate => true)
      redirect_to search_path
  end

  def by_matching
    Sorting.find_by(parent_id: current_user.parent.id).update_columns(:by_created_at => false,
                                                                      :by_matching => true,
                                                                      :by_rate => false)
    redirect_to search_path
  end
end
