class UsersController < ApplicationController
  before_action :logged_in_user,             only: [:edit, :update, :destroy,
                                                    :following, :followers, :show]
  before_action :correct_user,               only: [:edit, :update]
  before_action :correct_job,                only: :show
  before_action :access_permission_ratings,  only: :ratings
  before_action :access_permission_profiles, only: :show


  def ratings
    @user  = User.find(params[:id])
    @ratings_received = Rating.where("(receiver_id = " + @user.id.to_s + ")")
    @ratings_sent = Rating.where("(sender_id = " + @user.id.to_s + ")")

    render 'users/ratings'
  end


  def show

    @rating = Rating.new
    @ratings_received = Rating.where("(receiver_id = " + @user.id.to_s + ")")
    @user = User.find(params[:id])
    @conversations = Conversation.all

    if !@user.job.nil?
      @calendartimes = @user.job.time.split(",")
    end
    if !@user.parent.nil?
      @parent_calendartimes = @user.parent.time.split(",")
    end

    if parent?
      @potentials = current_user.parent.potential_ids
    else
      @potentials = []
    end

    if job?
      @interesteds = current_user.job.interested_ids
    else
      @interesteds = []
    end

    if !current_user.messages.nil?
      last_conversation = Conversation.where("(parent_user_id = " + current_user.id.to_s + ") or (job_user_id = " + current_user.id.to_s + ")").last
      if !last_conversation.nil?
        @last_message = last_conversation.messages.last
      end
    else
      @last_message = []
    end
  end


  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      # Mailer kann durch unkommentieren angesprochen werden
      # UserMailer.welcome_email(@user).deliver_now
      log_in(@user)
      flash[:success] = "Willkommen bei BabyRitter"
      redirect_back_or @user
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = "Userprofil aktualisiert"
      redirect_to @user
    else
      render 'edit'
    end
  end


    private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

   # availability only visible if matched
    def correct_job
      if (parent? && !current_user.parent.sorting.nil?)
        assigned_nanny_users = current_user.parent.sorting.helpfield.split(",")
        if assigned_nanny_users.include?(params[:id])
          @correct_nanny = true
        else
          @correct_nanny = false
        end
      end
    end
end


