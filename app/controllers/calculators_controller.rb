class CalculatorsController < ApplicationController
  before_action :logged_in_user


  def new
   @gams = Calculator.new
  end

  def create
    @gams = Calculator.create(gams_params)
    if @gams.save
      current_user.calculator = @gams
      flash[:success] = "GAMS Pfad gespeichert!"
      if current_user.admin?
        redirect_to current_user
      else
        redirect_to :controller => 'parents', :action => 'search'
      end
    else
      render 'new'
    end
  end


  private

  def gams_params
    params.require(:calculator).permit(:user_id, :gams)
  end

end
