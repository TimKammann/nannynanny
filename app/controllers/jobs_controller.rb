class JobsController < ApplicationController
  before_action :is_the_admin,       only: [:new, :create]
  before_action :job_redirect,       only: [:new, :create]
  before_action :only_nannies,       only: [:new, :create]
  before_action :is_a_nanny,         only: :edit
  before_action :only_admins,        only: [:index, :destroy]
  before_action :no_profile_reset,   only: :new
  before_action do
    if job?
      @times = current_user.job.time.split(",")
    end
  end


  def index
    nanny_users = Job.pluck(:user_id)
    @users = User.where(id: nanny_users)
  end

  def new
    @job = Job.new
  end

  def edit
  end

  def update
    if current_user.job.update_attributes(job_params)

      #alle offenen Anfragen löschen, wenn job editiert wird
      openRequests = Request.where("(paid = 'f') and (potential_id = " + current_user.job.id.to_s + ")")
      openRequests.each do |request|
        request.destroy
      end

      flash[:success] = "Nannyprofil aktualisiert"
      redirect_to current_user
    else
      render 'edit'
    end
  end

  def create
      @job = Job.create(job_params)
      if @job.save
        current_user.job = @job
        flash[:success] = "Nannyprofil gespeichert!"
        redirect_to current_user
      else
        render 'new'
      end
  end

  def destroy
    Job.find(params[:id]).destroy
    flash[:success] = "Nannyprofil entfernt!"
    redirect_to jobs_path
  end


  private

  def job_params
      params.require(:job).permit(:user_id, :valid_from, :valid_until, :time, :content, :nonsmoker, :homework, :city,
                                  :age, :nanny_gender, :gender, :kids, :baby, :toddler, :preschooler, :gradeschooler,
                                  :teenager, :experience, :rate, :first_aid, :babysitter_certificate, :pets, :licence,
                                  :car, :laundry, :run_errands, :to_bed, :cooking, :bodycare, :feed, :read, :music,
                                  :sports, :swimming, :handwork, :painting, :excursions)
  end
end

