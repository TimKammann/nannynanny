class MatchingsController < ApplicationController
  before_action :set_gams_path, only: :match
  before_action :only_admins

  def index
    @parents = Parent.all
    @parents_with_matching = Parent.where("(assigned > '0')")
  end


  def match

    @jobs = Job.all
    @parents = Parent.all
    parentSize = @parents.length
    jobSize = @jobs.length

#---number of nannies---#
    if File.exist?("match_Nannies.inc")
      File.delete("match_Nannies.inc")
    end
    f=File.new("match_Nannies.inc", "w")
    printf(f, "set n /\n")
    jobSize = @jobs.length
    jobCount = 0
    @jobs.each do |job|
      if (jobCount == jobSize - 1)
        printf(f, "n" + job.user_id.to_s + "\n")
      else
        printf(f, "n" + job.user_id.to_s + ",\n")
      end
      jobCount += 1
    end
    printf(f, "/;\n")
    f.close


#---number of parents---#
    if File.exist?("match_Parents.inc")
      File.delete("match_Parents.inc")
    end
    f=File.new("match_Parents.inc", "w")
    printf(f, "set p /\n")
    parentCount = 0
    @parents.each do |parent|
      if (parentCount == parentSize - 1)
        printf(f, "p" + parent.user_id.to_s + "\n")
      else
        printf(f, "p" + parent.user_id.to_s + ",\n")
      end
      parentCount += 1
    end
    printf(f, "/;\n")
    f.close


#---nannies_attributes---#
    if File.exist?("match_Nannies_Attributes.inc")
      File.delete("match_Nannies_Attributes.inc")
    end
    f=File.new("match_Nannies_Attributes.inc", "w")
    printf(f, "table d(n, m)\n")
    (0..14).each do |j|
    printf(f, "\tm" + j.to_s)
    end
    printf(f, "\tm15\n")
    @jobs.each do |job|
      printf(f, "n" + job.user_id.to_s + "\t")
      printf(f, "1" + "\t")
      printf(f, (job.homework ? 1 : 0).to_s + "\t")
      printf(f, (job.first_aid ? 1 : 0).to_s + "\t")
      printf(f, (job.babysitter_certificate ? 1 : 0).to_s + "\t")
      printf(f, (job.licence ? 1 : 0).to_s + "\t")
      printf(f, (job.car ? 1 : 0).to_s + "\t")
      printf(f, (job.run_errands ? 1 : 0).to_s + "\t")
      printf(f, (job.cooking ? 1 : 0).to_s + "\t")
      printf(f, (job.bodycare ? 1 : 0).to_s + "\t")
      printf(f, (job.feed ? 1 : 0).to_s + "\t")
      printf(f, (job.read ? 1 : 0).to_s + "\t")
      printf(f, (job.music ? 1 : 0).to_s + "\t")
      printf(f, (job.sports ? 1 : 0).to_s + "\t")
      printf(f, (job.swimming ? 1 : 0).to_s + "\t")
      printf(f, (job.painting ? 1 : 0).to_s + "\t")
      printf(f, (job.excursions ? 1 : 0).to_s + "\n")
    end
    f.close


#---parent_preferences---#
    if File.exist?("match_Parents_Preferences.inc")
      File.delete("match_Parents_Preferences.inc")
    end
    f=File.new("match_Parents_Preferences.inc", "w")
    printf(f, "table c(p,m) \n")
    (0..14).each do |j|
      printf(f, "\tm" + j.to_s)
    end
    printf(f, "\tm15\n")
    @parents.each do |parent|
      printf(f, "p" + parent.user_id.to_s + "\t")
      printf(f, "1" + "\t")
      printf(f, (parent.homework < 3 ? parent.homework.to_s : "0.0") + "\t")
      printf(f, (parent.first_aid < 3 ? parent.first_aid.to_s : "0.0") + "\t")
      printf(f, (parent.babysitter_certificate < 3 ? parent.babysitter_certificate.to_s : "0.0") + "\t")
      printf(f, (parent.licence < 3 ? parent.licence.to_s : "0.0") + "\t")
      printf(f, (parent.car < 3 ? parent.car.to_s : "0.0") + "\t")
      printf(f, (parent.run_errands < 3 ? parent.run_errands.to_s : "0.0") + "\t")
      printf(f, (parent.cooking < 3 ? parent.cooking.to_s : "0.0") + "\t")
      printf(f, (parent.bodycare < 3 ? parent.bodycare.to_s : "0.0") + "\t")
      printf(f, (parent.feed < 3 ? parent.feed.to_s : "0.0") + "\t")
      printf(f, (parent.read < 3 ? parent.read.to_s : "0.0") + "\t")
      printf(f, (parent.music < 3 ? parent.music.to_s : "0.0") + "\t")
      printf(f, (parent.sports < 3 ? parent.sports.to_s : "0.0") + "\t")
      printf(f, (parent.swimming < 3 ? parent.swimming.to_s : "0.0") + "\t")
      printf(f, (parent.painting < 3 ? parent.painting.to_s : "0.0") + "\t")
      printf(f, (parent.excursions < 3 ? parent.excursions.to_s : "0.0") + "\t")
      printf(f, "\n")
    end
    f.close
    

#---Filter Results---#

    if File.exist?("match_Filter_Results.inc")
      File.delete("match_Filter_Results.inc")
    end
    f=File.new("match_Filter_Results.inc", "w")
    printf(f, "set ok(p,n); \n")
    printf(f, "ok(p,n)=no; \n")


    @parents.each do |parent|
#---1st stage: attributes
    whereDB = ""
    firstElement = true
#Anzahl Kinder
    if (!firstElement)
      whereDB += "and (kids >= " + parent.kids.to_s + ")"
    else
      whereDB += "(kids >= " + parent.kids.to_s + ")"
      firstElement = false
    end
#Geschlecht der Kinder
    if (!firstElement)
      whereDB += "and ((gender = " + parent.gender.to_s + ") or (gender = 3))"
    else
      whereDB += "((gender = " + parent.gender.to_s + ") or (gender = 3))"
      firstElement = false
    end
#Alter der Kinder
    if (parent.baby?)
      if (!firstElement)
        whereDB += "and (baby = 't')"
      else
        whereDB += "(baby = 't')"
        firstElement = false
      end
    end
    if (parent.toddler?)
      if (!firstElement)
        whereDB += "and (toddler = 't')"
      else
        whereDB += "(toddler = 't')"
        firstElement = false
      end
    end
    if (parent.preschooler?)
      if (!firstElement)
        whereDB += "and (preschooler = 't')"
      else
        whereDB += "(preschooler = 't')"
        firstElement = false
      end
    end
    if (parent.gradeschooler?)
      if (!firstElement)
        whereDB += "and (gradeschooler = 't')"
      else
        whereDB += "(gradeschooler = 't')"
        firstElement = false
      end
    end
    if (parent.teenager?)
      if (!firstElement)
        whereDB += "and (teenager = 't')"
      else
        whereDB += "(teenager = 't')"
        firstElement = false
      end
    end
#Haustiere
    if (parent.pets?)
      if (!firstElement)
        whereDB += "and (pets = 't')"
      else
        whereDB += "(pets = 't')"
        firstElement = false
      end
    end
#Alter der Nanny
    if (!firstElement)
      whereDB += "and (age >= " + parent.lower_age.to_s + ")" + "and (age <= " + parent.upper_age.to_s + ")"
    else
      whereDB += "(age >= " + parent.lower_age.to_s + ")" + "and (age <= " + parent.upper_age.to_s + ")"
      firstElement = false
    end
#NannyGender
    if parent.nanny_gender < 3
      if (!firstElement)
        whereDB += "and (nanny_gender = " + parent.nanny_gender.to_s + ")"
      else
        whereDB += "(nanny_gender = " + parent.nanny_gender.to_s + ")"
        firstElement = false
      end
    end
#Stundenlohn
    if (!firstElement)
      whereDB += "and (rate >= " + parent.lower_rate.to_s + ")" + "and (rate <= " + parent.upper_rate.to_s + ")"
    else
      whereDB += "(rate >= " + parent.lower_rate.to_s + ")" + "and (rate <= " + parent.upper_rate.to_s + ")"
      firstElement = false
    end
# Nonsmoker
    if (parent.nonsmoker?)
      if (!firstElement)
        whereDB += "and (nonsmoker = 't')"
      else
        whereDB += "(nonsmoker = 't')"
        firstElement = false
      end
    end
#Erfahrung als Nanny
    if (!firstElement)
      whereDB += "and (experience >= " + parent.experience.to_s + ")"
    else
      whereDB += "(experience >= " + parent.experience.to_s + ")"
      firstElement = false
    end
# Fuehrerschein
    if (parent.licence? and (parent.licence > 2))
      if (!firstElement)
        whereDB += "and (licence = 't')"
      else
        whereDB += "(licence = 't')"
        firstElement = false
      end
    end
# Eigenes Auto
    if (parent.car? and (parent.car > 2))
      if (!firstElement)
        whereDB += "and (car = 't')"
      else
        whereDB += "(car = 't')"
        firstElement = false
      end
    end
# Erste-Hilfe
    if (parent.first_aid? and (parent.first_aid > 2))
      if (!firstElement)
        whereDB += "and (first_aid = 't')"
      else
        whereDB += "(first_aid = 't')"
        firstElement = false
      end
    end
# Babysitter Zertifikat
    if (parent.babysitter_certificate? and (parent.babysitter_certificate > 2))
      if (!firstElement)
        whereDB += "and (babysitter_certificate = 't')"
      else
        whereDB += "(babysitter_certificate = 't')"
        firstElement = false
      end
    end
# Hausaufgabenhilfe
    if (!parent.homework.nil? and (parent.homework > 2))
      if (!firstElement)
        whereDB += "and (homework = 't')"
      else
        whereDB += "(homework = 't')"
        firstElement = false
      end
    end
# Waesche
    if (!parent.laundry.nil? and (parent.laundry > 2))
      if (!firstElement)
        whereDB += "and (laundry = 't')"
      else
        whereDB += "(laundry = 't')"
        firstElement = false
      end
    end
# Besorgungen
    if (!parent.run_errands.nil? and (parent.run_errands > 2))
      if (!firstElement)
        whereDB += "and (run_errands = 't')"
      else
        whereDB += "(run_errands = 't')"
        firstElement = false
      end
    end
# Ins Bett bringen
    if (!parent.to_bed.nil? and (parent.to_bed > 2))
      if (!firstElement)
        whereDB += "and (to_bed = 't')"
      else
        whereDB += "(to_bed = 't')"
        firstElement = false
      end
    end
# Kochen
    if (!parent.cooking.nil? and (parent.cooking > 2))
      if (!firstElement)
        whereDB += "and (cooking = 't')"
      else
        whereDB += "(cooking = 't')"
        firstElement = false
      end
    end
# Koerperpflege
    if (!parent.bodycare.nil? and (parent.bodycare > 2))
      if (!firstElement)
        whereDB += "and (bodycare = 't')"
      else
        whereDB += "(bodycare = 't')"
        firstElement = false
      end
    end
# Fuettern
    if (!parent.feed.nil? and (parent.feed > 2))
      if (!firstElement)
        whereDB += "and (feed = 't')"
      else
        whereDB += "(feed = 't')"
        firstElement = false
      end
    end
# Vorlesen
    if (!parent.read.nil? and (parent.read > 2))
      if (!firstElement)
        whereDB += "and (read = 't')"
      else
        whereDB += "(read = 't')"
        firstElement = false
      end
    end
# Musik
    if (!parent.music.nil? and (parent.music > 2))
      if (!firstElement)
        whereDB += "and (music = 't')"
      else
        whereDB += "(music = 't')"
        firstElement = false
      end
    end
# Sport
    if (!parent.sports.nil? and (parent.sports > 2))
      if (!firstElement)
        whereDB += "and (sports = 't')"
      else
        whereDB += "(sports = 't')"
        firstElement = false
      end
    end
# Schwimmen
    if (!parent.swimming.nil? and (parent.swimming > 2))
      if (!firstElement)
        whereDB += "and (swimming = 't')"
      else
        whereDB += "(swimming = 't')"
        firstElement = false
      end
    end
# Malen und Zeichnen
    if (!parent.painting.nil? and (parent.painting > 2))
      if (!firstElement)
        whereDB += "and (painting = 't')"
      else
        whereDB += "(painting = 't')"
        firstElement = false
      end
    end
# Ausfluege
    if (!parent.excursions.nil? and (parent.excursions > 2))
      if (!firstElement)
        whereDB += "and (excursions = 't')"
      else
        whereDB += "(excursions = 't')"
        firstElement = false
      end
    end

      ok_jobs = Job.where(whereDB)


      #---availability filter---#
      #-VORGEHEN der Schleife
      # für alle parents
         # für alle jobs
         # speicher zeiten der momentanen nanny lokal in drei variablen
            # über alle bookings oder parents die schonmal bezahlt haben für die nanny
              # suche nach überschneidungen für momentanen parent und die parents die schonmal bezahlt haben
              # Überschneidung ? nimm bei der lokalen variable die geblockten zeiten des parents der bezahlt hat raus
            # schleife fertig
         # test ob momentaner parent anhand der lokalen variablen gematcht werden kann
         # schleife fertig
         # put
      # schleife fertig


      ok_jobs_parent_final = Array.new

      ok_jobs.each do |job|

        start_date_job = job.valid_from.tr('-', '').to_i
        end_date_job = job.valid_until.tr('-', '').to_i
        ok_times_job = job.time.split(",")

        bookings = Request.where("(paid = 't') and (potential_id = " + job.id.to_s + ")")

        bookings.each do |booking|

          start_date_bookedParent = booking.start_date.tr('-', '').to_i
          end_date_bookedParent = booking.end_date.tr('-', '').to_i
          ok_times_bookedParent = booking.time.split(",")

          if ( ((start_date_bookedParent >= start_date_job) and (start_date_bookedParent <= end_date_job)) or
              ((end_date_bookedParent >= start_date_job) and (end_date_bookedParent <= end_date_job)) or
              ((start_date_job >= start_date_bookedParent) and (end_date_job <= end_date_bookedParent)) or
              ((end_date_job >= start_date_bookedParent) and (end_date_job <= end_date_bookedParent)) )

            ok_times_bookedParent.each do |i|
              ok_times_job.delete i
            end
          end
        end

        parentdaytimes = parent.time.split(",")
        parentTimeRange_start = parent.valid_from.tr('-', '').to_i
        parentTimeRange_end = parent.valid_until.tr('-', '').to_i

        if (start_date_job <= parentTimeRange_start) and (parentTimeRange_start <= end_date_job) and
            (start_date_job <= parentTimeRange_end) and (parentTimeRange_end <= end_date_job)
          if (ok_times_job & parentdaytimes).length == parentdaytimes.length
            ok_jobs_parent_final.push(job.user_id)
          end
        end

      end

      if ok_jobs_parent_final.any?
        ok_jobs_parent_final.each do |id|
          printf(f, "ok('p" + parent.user_id.to_s + "','n" + id.to_s + "')=yes;" + "\n")
        end
      end

    end
    f.close


    # call GAMS and set matches to parents.assigned
    if File.exist?("many_to_many_results.txt")
      File.delete("many_to_many_results.txt")
    end
    system current_user.calculator.gams + " many_to_many"

    # read results
    if File.exist?("many_to_many_results.txt")
      f=File.open("many_to_many_results.txt", "r")
      f.each { |line|
        one_to_one = line.split(",")
        parent_user = one_to_one[0].delete "p"
        nanny_user = one_to_one[1].delete "n"
        Parent.find_by(user_id: parent_user.to_i).update_column(:assigned, nanny_user.to_i)
      }
      f.close
    end

    redirect_to index_path
  end
end

