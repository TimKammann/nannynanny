class RatingsController < ApplicationController
  before_action :logged_in_user


  def create
    @rating = Rating.create(rating_params)
    if @rating.save
      flash[:success] = "Bewertung abgegeben."
      redirect_to current_user
    else
      redirect_to current_user
    end
  end


  private

  def rating_params
    params.require(:rating).permit(:sender_id, :receiver_id, :content, :parent_tidiness, :parent_correctness,
                                 :parent_kindness, :parent_recommendation, :job_punctuality, :job_satisfaction_of_child,
                                 :job_correctness, :job_recommendation)
  end
end
