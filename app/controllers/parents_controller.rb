class ParentsController < ApplicationController
  before_action :is_the_admin,     only: [:new, :create]
  before_action :parent_redirect,  only: [:new, :create]
  before_action :is_a_parent,      only: [:optimize, :search]
  before_action :only_parents,     only: [:new, :create]
  before_action :set_gams_path,    only: [:optimize, :search]
  before_action :is_premium,       only: [:optimize, :search]
  before_action :only_admins,      only: [:index, :destroy]
  before_action :no_profile_reset, only: :new
  before_action do
    if parent?
      @times = current_user.parent.time.split(",")
    end
  end

  def index
    parent_users = Parent.pluck(:user_id)
    @users = User.where(id: parent_users)
  end

  def new
    @parent = Parent.new
  end

  def edit
  end

  def update
    if current_user.parent.update_attributes(parent_params)

      # alle offenen Anfragen löschen, wenn parent editiert wird
      openRequests = Request.where("(paid = 'f') and (interested_id = " + current_user.parent.id.to_s + ")")
      openRequests.each do |request|
        request.destroy
      end

      # one-to-many: Matchings löschen
      current_user.parent.assigned = 0
      current_user.parent.save

      # many-to-many: Matchings löschen
      if !current_user.parent.sorting.nil?
        current_user.parent.sorting.destroy
      end

      flash[:success] = "Elternprofil aktualisiert"
      redirect_to current_user
    else
      render 'edit'
    end
  end

  def search
    if !current_user.parent.sorting.nil?
      a = current_user.parent.sorting.helpfield.split(",")
      b = Array[ *a.collect { |v| [ Jobfind(v)] }.flatten ]
      c = current_user.parent.sorting.helpfield2.split(",").flatten.collect { |i| i.to_f}

      # sorted array for loop in view default on matching
      if current_user.parent.sorting.by_matching?
        @assigned_nannies = b.reverse
        @assigned_nannies_value = c.reverse
      end

      # arrays: values and nannies -> sortieren je nach auswahl
      if current_user.parent.sorting.by_created_at?
        @assigned_nannies = b.sort_by { |obj| obj.created_at }.reverse

        buffer_nannies_value = Array.new(b.size, 0.0)
        buffer_index = Array.new(b.size, 0)

        for i in 0..b.size-1 do
          buffer_index[i] = @assigned_nannies.index(b[i])
        end
        for i in 0..b.size-1 do
          buffer_nannies_value[buffer_index[i]] = c[i]
        end
        @assigned_nannies_value = buffer_nannies_value
      end
      if current_user.parent.sorting.by_rate?
        @assigned_nannies = b.sort_by { |obj| obj.rate }

        buffer_nannies_value = Array.new(b.size, 0.0)
        buffer_index = Array.new(b.size, 0)

        for i in 0..b.size-1 do
          buffer_index[i] = @assigned_nannies.index(b[i])
        end
        for i in 0..b.size-1 do
          buffer_nannies_value[buffer_index[i]] = c[i]
        end
        @assigned_nannies_value = buffer_nannies_value
      end

    else
      @assigned_nannies = []
      @assigned_nannies_value = []
    end
  end

  def create
    @parent = Parent.create(parent_params)
    if @parent.save
      current_user.parent = @parent
      flash[:success] = "Elternprofil angelegt!"
      redirect_to current_user
    else
      render 'new'
    end
  end

  def destroy
    Parent.find(params[:id]).destroy
    flash[:success] = "Elternprofil entfernt!"
    redirect_to parents_path
  end


  def optimize

#----------Datenbankfilter BEGINN----------#

 #---: attributes
    whereDB = ""
    firstElement = true
    #Anzahl Kinder
    if (!firstElement)
      whereDB += "and (kids >= " + current_user.parent.kids.to_s + ")"
    else
      whereDB += "(kids >= " + current_user.parent.kids.to_s + ")"
      firstElement = false
    end
    #Geschlecht der Kinder
    if (!firstElement)
      whereDB += "and ((gender = " + current_user.parent.gender.to_s + ") or (gender = 3))"
    else
      whereDB += "((gender = " + current_user.parent.gender.to_s + ") or (gender = 3))"
      firstElement = false
    end
    #Alter der Kinder
    if (current_user.parent.baby?)
      if (!firstElement)
        whereDB += "and (baby = 't')"
      else
        whereDB += "(baby = 't')"
        firstElement = false
      end
    end
    if (current_user.parent.toddler?)
      if (!firstElement)
        whereDB += "and (toddler = 't')"
      else
        whereDB += "(toddler = 't')"
        firstElement = false
      end
    end
    if (current_user.parent.preschooler?)
      if (!firstElement)
        whereDB += "and (preschooler = 't')"
      else
        whereDB += "(preschooler = 't')"
        firstElement = false
      end
    end
    if (current_user.parent.gradeschooler?)
      if (!firstElement)
        whereDB += "and (gradeschooler = 't')"
      else
        whereDB += "(gradeschooler = 't')"
        firstElement = false
      end
    end
    if (current_user.parent.teenager?)
      if (!firstElement)
        whereDB += "and (teenager = 't')"
      else
        whereDB += "(teenager = 't')"
        firstElement = false
      end
    end
    #Haustiere
    if (current_user.parent.pets?)
      if (!firstElement)
        whereDB += "and (pets = 't')"
      else
        whereDB += "(pets = 't')"
        firstElement = false
      end
    end
    #Alter der Nanny
    if (!firstElement)
      whereDB += "and (age >= " + current_user.parent.lower_age.to_s + ")" + "and (age <= " + current_user.parent.upper_age.to_s + ")"
    else
      whereDB += "(age >= " + current_user.parent.lower_age.to_s + ")" + "and (age <= " + current_user.parent.upper_age.to_s + ")"
      firstElement = false
    end
    #NannyGender
    if current_user.parent.nanny_gender < 3
      if (!firstElement)
        whereDB += "and (nanny_gender = " + current_user.parent.nanny_gender.to_s + ")"
      else
        whereDB += "(nanny_gender = " + current_user.parent.nanny_gender.to_s + ")"
        firstElement = false
      end
    end
    #Stundenlohn
    if (!firstElement)
      whereDB += "and (rate >= " + current_user.parent.lower_rate.to_s + ")" + "and (rate <= " + current_user.parent.upper_rate.to_s + ")"
    else
      whereDB += "(rate >= " + current_user.parent.lower_rate.to_s + ")" + "and (rate <= " + current_user.parent.upper_rate.to_s + ")"
      firstElement = false
    end
    # Nonsmoker
    if (current_user.parent.nonsmoker?)
      if (!firstElement)
        whereDB += "and (nonsmoker = 't')"
      else
        whereDB += "(nonsmoker = 't')"
        firstElement = false
      end
    end
    #Erfahrung als Nanny
    if (!firstElement)
      whereDB += "and (experience >= " + current_user.parent.experience.to_s + ")"
    else
      whereDB += "(experience >= " + current_user.parent.experience.to_s + ")"
      firstElement = false
    end
    # Fuehrerschein
    if (current_user.parent.licence? and (current_user.parent.licence > 2))
      if (!firstElement)
        whereDB += "and (licence = 't')"
      else
        whereDB += "(licence = 't')"
        firstElement = false
      end
    end
    # Eigenes Auto
    if (current_user.parent.car? and (current_user.parent.car > 2))
      if (!firstElement)
        whereDB += "and (car = 't')"
      else
        whereDB += "(car = 't')"
        firstElement = false
      end
    end
    # Erste-Hilfe
    if (current_user.parent.first_aid? and (current_user.parent.first_aid > 2))
      if (!firstElement)
        whereDB += "and (first_aid = 't')"
      else
        whereDB += "(first_aid = 't')"
        firstElement = false
      end
    end
    # Babysitter Zertifikat
    if (current_user.parent.babysitter_certificate? and (current_user.parent.babysitter_certificate > 2))
      if (!firstElement)
        whereDB += "and (babysitter_certificate = 't')"
      else
        whereDB += "(babysitter_certificate = 't')"
        firstElement = false
      end
    end
    # Homework
    if (!current_user.parent.homework.nil? and (current_user.parent.homework > 2))
      if (!firstElement)
        whereDB += "and (homework = 't')"
      else
        whereDB += "(homework = 't')"
        firstElement = false
      end
    end
    # Waesche
    if (!current_user.parent.laundry.nil? and (current_user.parent.laundry > 2))
      if (!firstElement)
        whereDB += "and (laundry = 't')"
      else
        whereDB += "(laundry = 't')"
        firstElement = false
      end
    end
    # Besorgungen
    if (!current_user.parent.run_errands.nil? and (current_user.parent.run_errands > 2))
      if (!firstElement)
        whereDB += "and (run_errands = 't')"
      else
        whereDB += "(run_errands = 't')"
        firstElement = false
      end
    end
    # Ins Bett bringen
    if (!current_user.parent.to_bed.nil? and (current_user.parent.to_bed > 2))
      if (!firstElement)
        whereDB += "and (to_bed = 't')"
      else
        whereDB += "(to_bed = 't')"
        firstElement = false
      end
    end
    # Kochen
    if (!current_user.parent.cooking.nil? and (current_user.parent.cooking > 2))
      if (!firstElement)
        whereDB += "and (cooking = 't')"
      else
        whereDB += "(cooking = 't')"
        firstElement = false
      end
    end
    # Koerperpflege
    if (!current_user.parent.bodycare.nil? and (current_user.parent.bodycare > 2))
      if (!firstElement)
        whereDB += "and (bodycare = 't')"
      else
        whereDB += "(bodycare = 't')"
        firstElement = false
      end
    end
    # Fuettern
    if (!current_user.parent.feed.nil? and (current_user.parent.feed > 2))
      if (!firstElement)
        whereDB += "and (feed = 't')"
      else
        whereDB += "(feed = 't')"
        firstElement = false
      end
    end
    # Vorlesen
    if (!current_user.parent.read.nil? and (current_user.parent.read > 2))
      if (!firstElement)
        whereDB += "and (read = 't')"
      else
        whereDB += "(read = 't')"
        firstElement = false
      end
    end
    # Musik
    if (!current_user.parent.music.nil? and (current_user.parent.music > 2))
      if (!firstElement)
        whereDB += "and (music = 't')"
      else
        whereDB += "(music = 't')"
        firstElement = false
      end
    end
    # Sport
    if (!current_user.parent.sports.nil? and (current_user.parent.sports > 2))
      if (!firstElement)
        whereDB += "and (sports = 't')"
      else
        whereDB += "(sports = 't')"
        firstElement = false
      end
    end
    # Schwimmen
    if (!current_user.parent.swimming.nil? and (current_user.parent.swimming > 2))
      if (!firstElement)
        whereDB += "and (swimming = 't')"
      else
        whereDB += "(swimming = 't')"
        firstElement = false
      end
    end
    # Malen und Zeichnen
    if (!current_user.parent.painting.nil? and (current_user.parent.painting > 2))
      if (!firstElement)
        whereDB += "and (painting = 't')"
      else
        whereDB += "(painting = 't')"
        firstElement = false
      end
    end
    # Ausfluege
    if (!current_user.parent.excursions.nil? and (current_user.parent.excursions > 2))
      if (!firstElement)
        whereDB += "and (excursions = 't')"
      else
        whereDB += "(excursions = 't')"
        firstElement = false
      end
    end
    # nicht sich selber auswählen wenn Parent und Job in einem Account
    if (!firstElement)
      whereDB += "and (user_id <> " + current_user.id.to_s + ")"
    else
      whereDB += "(user_id <> " + current_user.id.to_s + ")"
    end
 # 1st stage: output
    @jobs = Job.where(whereDB)



    #---: availability filter
    #-VORGEHEN der Schleife
    # für den current_user.parent
      # für alle jobs
      # speicher zeiten lokal in drei variablen
        # über alle bookings oder parents die schonmal bezahlt haben für die nanny
          # suche nach überschneidungen für momentanen parent und die parents die schonmal bezahlt haben
          # Überschneidung ? nimm bei der lokalen variable die geblockten zeiten des parents der bezahlt hat raus
        # schleife fertig
      # test ob momentaner parent anhand der lokalen variablen gematcht werden kann
      # schleife fertig
      # put
    # schleife fertig

    ok_jobs_parent_final = Array.new

    @jobs.each do |job|

      start_date_job = job.valid_from.tr('-', '').to_i
      end_date_job = job.valid_until.tr('-', '').to_i
      ok_times_job = job.time.split(",")

      bookings = Request.where("(paid = 't') and (potential_id = " + job.id.to_s + ")")

      bookings.each do |booking|

        start_date_bookedParent = booking.start_date.tr('-', '').to_i
        end_date_bookedParent = booking.end_date.tr('-', '').to_i
        ok_times_bookedParent = booking.time.split(",")

        if ( ((start_date_bookedParent >= start_date_job) and (start_date_bookedParent <= end_date_job)) or
            ((end_date_bookedParent >= start_date_job) and (end_date_bookedParent <= end_date_job)) or
            ((start_date_job >= start_date_bookedParent) and (end_date_job <= end_date_bookedParent)) or
            ((end_date_job >= start_date_bookedParent) and (end_date_job <= end_date_bookedParent)) )

          ok_times_bookedParent.each do |i|
            ok_times_job.delete i
          end
        end
      end

      parentDaytimes = current_user.parent.time.split(",")
      parentTimeRange_start = current_user.parent.valid_from.tr('-', '').to_i
      parentTimeRange_end = current_user.parent.valid_until.tr('-', '').to_i

      if (start_date_job <= parentTimeRange_start) and (parentTimeRange_start <= end_date_job) and
          (start_date_job <= parentTimeRange_end) and (parentTimeRange_end <= end_date_job)

        if (ok_times_job & parentDaytimes).length == parentDaytimes.length
          ok_jobs_parent_final.push(job)
        end
      end

    end

    @jobs = Job.where(id: ok_jobs_parent_final)

#----------Datenbankfilter ENDE----------#

    #wenn keine Nannies den Mindestanforderungen entsprechen dann hier Abbruch
    if @jobs.empty?
      flash[:info] = "Leider wurde keine Nanny gefunden, die Ihre Mindestanforderungen erfüllt! Eventuell Angaben im Elternprofil anpassen."
      redirect_to current_user
    else

#----------Output fuer GAMS BEGINN---------#

      #---: number of nannies
      if File.exist?("Nanny.inc")
          File.delete("Nanny.inc")
      end
      f=File.new("Nanny.inc", "w")
      printf(f, "set n /\n")
      jobSize = @jobs.length
      jobCount = 0
      @jobs.each do |job|
          if (jobCount == jobSize - 1)
              printf(f, "n" + job.user_id.to_s + "\n")
          else
              printf(f, "n" + job.user_id.to_s + ",\n")
          end
          jobCount += 1
      end
      printf(f, "/;\n")
      f.close


      #---: nanny attributes
      if File.exist?("NannyAttributes.inc")
          File.delete("NannyAttributes.inc")
      end
      f=File.new("NannyAttributes.inc", "w")
      printf(f, "table d(n, m)\n")
      (0..14).each do |j|
        printf(f, "\tm" + j.to_s)
      end
      printf(f, "\tm15\n")
      @jobs.each do |job|
          printf(f, "n" + job.user_id.to_s + "\t")
          printf(f, "1" + "\t")
          printf(f, (job.homework ? 1 : 0).to_s + "\t")
          printf(f, (job.first_aid ? 1 : 0).to_s + "\t")
          printf(f, (job.babysitter_certificate ? 1 : 0).to_s + "\t")
          printf(f, (job.licence ? 1 : 0).to_s + "\t")
          printf(f, (job.car ? 1 : 0).to_s + "\t")
          printf(f, (job.run_errands ? 1 : 0).to_s + "\t")
          printf(f, (job.cooking ? 1 : 0).to_s + "\t")
          printf(f, (job.bodycare ? 1 : 0).to_s + "\t")
          printf(f, (job.feed ? 1 : 0).to_s + "\t")
          printf(f, (job.read ? 1 : 0).to_s + "\t")
          printf(f, (job.music ? 1 : 0).to_s + "\t")
          printf(f, (job.sports ? 1 : 0).to_s + "\t")
          printf(f, (job.swimming ? 1 : 0).to_s + "\t")
          printf(f, (job.painting ? 1 : 0).to_s + "\t")
          printf(f, (job.excursions ? 1 : 0).to_s + "\n")
      end
      f.close

      #---: parent preferences
      if File.exist?("Preferences.inc")
          File.delete("Preferences.inc")
      end
      f=File.new("Preferences.inc", "w")
      printf(f, "parameter c(m) /\n")
      # m0 ist ein Dummy (für GAMS p(n) > 0 wenn alle Attribute = 0)
          printf(f, "m0 " + "1" + "\n")
          printf(f, "m1 " + (current_user.parent.homework < 3 ? current_user.parent.homework.to_s : "0.0") + "\n")
          printf(f, "m2 " + (current_user.parent.first_aid < 3 ? current_user.parent.first_aid.to_s : "0.0") + "\n")
          printf(f, "m3 " + (current_user.parent.babysitter_certificate < 3 ? current_user.parent.babysitter_certificate.to_s : "0.0") + "\n")
          printf(f, "m4 " + (current_user.parent.licence < 3 ? current_user.parent.licence.to_s : "0.0") + "\n")
          printf(f, "m5 " + (current_user.parent.car < 3 ? current_user.parent.car.to_s : "0.0") + "\n")
          printf(f, "m6 " + (current_user.parent.run_errands < 3 ? current_user.parent.run_errands.to_s : "0.0") + "\n")
          printf(f, "m7 " + (current_user.parent.cooking < 3 ? current_user.parent.cooking.to_s : "0.0") + "\n")
          printf(f, "m8 " + (current_user.parent.bodycare < 3 ? current_user.parent.bodycare.to_s : "0.0") + "\n")
          printf(f, "m9 " + (current_user.parent.feed < 3 ? current_user.parent.feed.to_s : "0.0") + "\n")
          printf(f, "m10 " + (current_user.parent.read < 3 ? current_user.parent.read.to_s : "0.0") + "\n")
          printf(f, "m11 " + (current_user.parent.music < 3 ? current_user.parent.music.to_s : "0.0") + "\n")
          printf(f, "m12 " + (current_user.parent.sports < 3 ? current_user.parent.sports.to_s : "0.0") + "\n")
          printf(f, "m13 " + (current_user.parent.swimming < 3 ? current_user.parent.swimming.to_s : "0.0") + "\n")
          printf(f, "m14 " + (current_user.parent.painting < 3 ? current_user.parent.painting.to_s : "0.0") + "\n")
          printf(f, "m15 " + (current_user.parent.excursions < 3 ? current_user.parent.excursions.to_s : "0.0") + "\n")
      printf(f, "/;\n")
      f.close

    #---: number of nannies to be assigned
    if File.exist?("searchsize.inc")
      File.delete("searchsize.inc")
    end
    f=File.new("searchsize.inc", "w")
    printf(f, "parameter x /\n")
    if (params[:searchsize] == "")
      searchsize = 1
    else
      searchsize = params[:searchsize].to_i
      if (searchsize <= 0)
        searchsize = 1
      end
      if (searchsize > 10)
        searchsize = 5
      end
    end
      # maximal gefunden:
    if @jobs.size <= searchsize
      searchsize = @jobs.size
    end
    printf(f, searchsize.to_s + "\n/;")
    f.close

#----------Output fuer GAMS ENDE---------#






#----------GAMS----------#

    if File.exist?("matching_results.txt")
        File.delete("matching_results.txt")
    end
      system current_user.calculator.gams + " one_to_many"
      #---: timeout open
      abort = 0
      while ((abort <= 80) && (!File.exist?("matching_results.txt"))) do
        sleep(0.25)
        abort += 1
      end
      #---assigned_nannies---#
      if File.exist?("matching_results.txt")
          fi=File.open("matching_results.txt", "r")
          line=fi.readline
          assigned_nannies = Array.new(searchsize, "")
          assigned_nannies_value_string = Array.new(searchsize, "")
          currentnanny = -1
          nannyfound=false
          @assigned_job=line
          @assigned_job.split("").each do |n|
            if ((n == ' ') || (n == "\n"))
              nannyfound = false
            else
              if (nannyfound)
                assigned_nannies[currentnanny] += n
              else
                if (n == 'n')
                  nannyfound = true
                  currentnanny += 1
                  assigned_nannies[currentnanny] = ""
                  assigned_nannies_value_string[currentnanny] = ""
                else
                  if ((n != ',') && (n != "\n"))
                    assigned_nannies_value_string[currentnanny] += n
                  end
                end
              end
            end
          end
          fi.close

          #---: maximum matching value
          attr = ["licence", "car", "first_aid", "babysitter_certificate", "homework", "laundry", "run_errands",
                  "to_bed", "cooking", "bodycare", "feed", "read", "music", "sports", "swimming", "painting", "excursions"]
          maxmatching = 1
          @current_user.parent.attributes.each do |attr_name, attr_value|
            if (attr.include?(attr_name))
              if attr_value < 3
                maxmatching = maxmatching + attr_value
              end
            end
          end
          assigned_nannies_value = Array.new(searchsize, 0.0)
          for i in 0..searchsize-1
            assigned_nannies_value[i] = ((assigned_nannies_value_string[i].to_f / maxmatching)*100).round
          end
          #---> result: sorted array of matching values
          assigned_nannies_value, assigned_nannies = assigned_nannies_value.zip(assigned_nannies).sort.transpose

      else
        assigned_nannies=nil
        @assigned_job=nil
        #---:timeout close
        if abort > 80
          flash.now[:not_available] = "Die Matchingsuche dauert länger als 20sec. Falscher GAMS-Pfad?"
        end
      end

    #--- Sorting create default on relative matching---#
    if !current_user.parent.sorting.nil?
      current_user.parent.sorting.update_column(:helpfield,
                                                assigned_nannies.map {|i| i.to_s }.join(","))
      current_user.parent.sorting.update_column(:helpfield2,
                                                assigned_nannies_value.map {|i| i.to_s }.join(","))
    else
      Sorting.create(:parent_id => Parent.find_by(user_id: current_user.id).id,
                     :helpfield => assigned_nannies.map {|i| i.to_s }.join(","),
                     :helpfield2 => assigned_nannies_value.map {|i| i.to_s }.join(","),
                     :by_matching => true )
    end
     #---> result: @assigned_nannies as array of sorted Jobs of class Active Record Relation
    @assigned_nannies = Array[ *assigned_nannies.collect { |v| [ Jobfind(v)] }.flatten ]
    @assigned_nannies_value = assigned_nannies_value

      redirect_to search_path
    end
  end



# # #

  private

  def parent_params
      params.require(:parent).permit(:user_id, :valid_from, :valid_until, :time, :content, :nonsmoker, :homework, :pets,
                                     :city, :address, :lower_age, :upper_age, :gender, :kids, :baby, :toddler,
                                     :preschooler, :gradeschooler, :teenager, :experience, :lower_rate, :upper_rate,
                                     :first_aid, :babysitter_certificate, :licence, :car, :laundry, :run_errands,
                                     :to_bed, :cooking, :bodycare, :feed, :read, :music, :sports, :swimming,
                                     :painting, :excursions, :nanny_gender, :assigned)
  end
end







