class StaticPagesController < ApplicationController
  before_action :is_a_parent, only: :premium
  before_filter :disable_footer, only: [:landing_page]


  def home
    @disable_footer = true
  end

  def help
  end

  def about
  end

  def contact
  end

  def proto_premium
  end

  def premium
    current_user.toggle!(:premium)
    redirect_to current_user
  end


end
