class RelationshipsController < ApplicationController
  before_action :logged_in_user
  before_action :is_premium, only: [:create, :destroy]

  def create
    @user = User.find(params[:followed_id])

    if current_user.following.count > 10
      flash[:info] = "Sie können maximal zehn Nannies in Ihre Merkliste aufnehmen."
      if params[:redirecter] == "back_to_user"
        redirect_to @user
      else
        redirect_to search_path
      end
    else
      current_user.follow(@user)
      if params[:redirecter] == "back_to_user"
        redirect_to @user
      else
      redirect_to search_path
      end
    end
  end

  def destroy
    current_user.active_relationships.find(params[:id]).destroy
    if params[:redirecter] == "back_to_user"
      redirect_to User.find(params[:the_requested_user])
    else
      redirect_to search_path
    end
  end
end
