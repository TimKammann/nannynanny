class RequestsController < ApplicationController
  before_action :logged_in_user
  before_action :is_a_parent, only: [:create, :pay]
  before_action :is_a_nanny, only: [:accept]

  require 'date'


  def create
    # bevor eine Anfrage verschickt wird, muss überprüft werden, ob in der Zwischenzeit (Matching bis Payment)
    # kein anderer Parent die Nanny für die gewünschte Zeit gebucht hat
    # --> Bookings (Requests where paid = true and potential_id = job in question)
    # Verfügbarkeit lokal aktualisieren und mit Daten von current_user.parent abgleichen

    # check source route
    if params[:redirecter] == 'back_to_user'
      job = User.find(params[:the_requested_user]).job
    else
      job = Job.find(params[:the_requested_user])
    end

    ok_jobs_parent_final = Array.new

      start_date_job = job.valid_from.tr('-', '').to_i
      end_date_job = job.valid_until.tr('-', '').to_i
      ok_times_job = job.time.split(",")

    bookings = Request.where("(paid = 't') and (potential_id = " + job.id.to_s + ")")

    bookings.each do |booking|

        start_date_bookedParent = booking.start_date.tr('-', '').to_i
        end_date_bookedParent = booking.end_date.tr('-', '').to_i
        ok_times_bookedParent = booking.time.split(",")

        if ( ((start_date_bookedParent >= start_date_job) and (start_date_bookedParent <= end_date_job)) or
            ((end_date_bookedParent >= start_date_job) and (end_date_bookedParent <= end_date_job)) or
            ((start_date_job >= start_date_bookedParent) and (end_date_job <= end_date_bookedParent)) or
            ((end_date_job >= start_date_bookedParent) and (end_date_job <= end_date_bookedParent)) )

          ok_times_bookedParent.each do |i|
            ok_times_job.delete i
          end
        end
      end

      parentdaytimes = current_user.parent.time.split(",")
      parentTimeRange_start = current_user.parent.valid_from.tr('-', '').to_i
      parentTimeRange_end = current_user.parent.valid_until.tr('-', '').to_i

      if (start_date_job <= parentTimeRange_start) and (parentTimeRange_start <= end_date_job) and
          (start_date_job <= parentTimeRange_end) and (parentTimeRange_end <= end_date_job)

        if (ok_times_job & parentdaytimes).length == parentdaytimes.length
          ok_jobs_parent_final.push(job)
        end
      end

    if Job.where(id: ok_jobs_parent_final).any?
      current_user.parent.active_requests.create(request_params)
      # Mailer kann durch unkommentieren angesprochen werden
      # UserMailer.parent_request_email(job.user).deliver_now

      if params[:redirecter] == "back_to_user"
        redirect_to User.find(params[:the_requested_user])
      else
        redirect_to search_path
      end
    else

      if current_user.premium?

        if (!current_user.active_relationships.nil? and
            !current_user.active_relationships.find_by(followed_id: params[:the_requested_user]).nil? and
            !current_user.parent.sorting.nil? and
            !current_user.parent.sorting.helpfield.split(",").include?(params[:the_requested_user].to_s))

          today = ("20" + Time.now.strftime("%y%m%d")).to_i
          if parentTimeRange_start < today
            flash[:info] = "Der Startzeitpunkt ihrer Nannysuche liegt in der Vergangenheit. Bitte aktualisieren Sie Ihr Profil."
            redirect_to current_user
          else
            flash[:info] = "Sie möchten eine Anfrage an eine Nanny aus ihrer Merkliste schicken. Leider stimmt ihr Bedarfszeitraum nicht mit der Verfügbarkeit der Nanny überein. Bitte passen Sie ggf. Ihr Elternprofil an."
            redirect_to current_user
          end

        else
          flash[:info] = "Leider wurde die Nanny soeben von anderen Eltern gebucht oder die Zeitangaben haben sich geändert."
          redirect_to current_user
        end

      else
      flash[:info] = "Leider wurde die Nanny soeben von anderen Eltern gebucht oder hat ihre Verfügbarkeit aktualisiert. Haben Sie schon über einen Premiumaccount nachgedacht?
                      Sie haben u.a. die Möglichkeit mehrere Anfragen zu verschicken."
      redirect_to current_user
      end
    end
  end

  def destroy
    Request.find(params[:id]).destroy
    if params[:redirecter] == "back_to_user"
      redirect_to User.find(params[:the_requested_user])
    else
      redirect_to search_path
    end
  end

  def accept
    request_to_accept = Request.find_by(id: (params[:request_id]))
    request_to_accept.update_column(:accepted, true)

    # wenn zeitl. Überschneidungen mit anderen offenen Anfragen, dann kollidierende Anfragen löschen
    # VORGEHEN (hier Check zwischen Zeiten von mehreren Parents, nicht Job und Parent)
    # für alle alle requests mit accepted = f von user.job (die ungleich request_to_accept)
      # über alle Interessenten (parents mit Anfragen zu current_user.job)
         # speicher gewünschte Zeiten des Interessenten lokal in drei Variablen
         # Zeiten des parent_to_accept und Zeiten des Interessenten kollidieren?
         # wenn ja? dann lösche kollidierende Anfragen
      # schleife fertig
    # schleife fertig

    parent_to_accept = Parent.find_by(id: request_to_accept.interested_id)

    start_date_parent_to_accept = parent_to_accept.valid_from.tr('-', '').to_i
    end_date_parent_to_accept = parent_to_accept.valid_until.tr('-', '').to_i
    ok_times_parent_to_accept = parent_to_accept.time.split(",")

    open_requests = current_user.job.passive_requests.where("(accepted = 'f')") - [request_to_accept]

    if open_requests.any?

     open_requests.each do |open_request|

       other_parent = Parent.find_by(id: open_request.interested_id)

       start_date_other_parent = other_parent.valid_from.tr('-', '').to_i
       end_date_other_parent = other_parent.valid_until.tr('-', '').to_i
       ok_times_other_parent = other_parent.time.split(",")

       if ( ((start_date_other_parent >= start_date_parent_to_accept) and (start_date_other_parent <= end_date_parent_to_accept)) or
           ((end_date_other_parent >= start_date_parent_to_accept) and (end_date_other_parent <= end_date_parent_to_accept)) or
           ((start_date_parent_to_accept >= start_date_other_parent) and (end_date_parent_to_accept <= end_date_other_parent)) or
           ((end_date_parent_to_accept >= start_date_other_parent) and (end_date_parent_to_accept <= end_date_other_parent)) )

         if !(ok_times_parent_to_accept & ok_times_other_parent).empty?

           open_request.destroy
         end
       end
     end
     flash[:info] = "Die Anfrage wurde akzeptiert. Offene Anfragen mit anderen Eltern wurden wegen zeitlicher Überschneidung gelöscht."
     redirect_to current_user

    else
      flash[:info] = "Die Anfrage wurde akzeptiert."
      redirect_to current_user
    end
  end


  def pay
    Request.find_by(id: (params[:request_id])).update_column(:paid, true)

    # alle anderen offenen Anfragen des parents löschen, wenn bezahlt
    quietRequests = Request.where("(paid = 'f') and (interested_id = " + current_user.parent.id.to_s + ")")
    quietRequests.each do |request|
      request.destroy
    end

    # current_user.parent.sorting löschen, wenn premium und bezahlt
    # (damit Premiumsuche beim nächsten mal aktualisiert ist und gebuchte Nanny nicht angezeigt)
    if (current_user.premium? && !current_user.parent.sorting.nil?)
      current_user.parent.sorting.destroy
    end

    redirect_to proto_pay_path
  end


   private

   def request_params
     # Anfrage nur in eine Richtung erstellbar
     params.require(:request).permit(:potential_id, :start_date, :end_date, :time)
   end

end