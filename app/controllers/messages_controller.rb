class MessagesController < ApplicationController
  before_action :logged_in_user
  before_action do
    @conversation = Conversation.find(params[:conversation_id])
  end

  def index
    @messages = @conversation.messages
    if @messages.length > 5
      @exceeds_five = true
      @messages = @messages[-5..-1]
    end
    if params[:m]
      @exceeds_five = false
      @messages = @conversation.messages
    end
    @message = @conversation.messages.new
  end

  def new
    @message = @conversation.messages.new
  end

  def create
    @message = @conversation.messages.new(message_params)
    if @message.save
      redirect_to conversation_messages_path(@conversation)
    else
      @messages = Conversation.find(params[:conversation_id]).messages
      render 'index'
    end
  end


  private

  def message_params
    params.require(:message).permit(:content, :user_id)
  end

end
