class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  include ParentsHelper
  include JobsHelper

  private

  def job_redirect
    unless (logged_in?)
      store_location
      redirect_to signup_path
    end
  end

  def parent_redirect
    unless (logged_in?)
      store_location
      redirect_to signup_path
    end
  end


  def landing_page
    @disable_footer = true
  end

  def logged_in_user
    unless logged_in?
      store_location
      flash[:info] = "Bitte zunächst einloggen."
      redirect_to login_url
    end
  end

   def set_gams_path
    unless !current_user.calculator.nil?
      flash[:info] = "Bitte zunächst GAMS Pfad festlegen."
      redirect_to calculators_url
    end
  end

  def is_a_parent
    unless parent?
      flash[:info] = "Diese Funktion steht nur Eltern zur Verfügung."
      redirect_to root_url
    end
  end

  def is_a_nanny
    unless job?
      flash[:info] = "Diese Funktion steht nur Nannies zur Verfügung."
      redirect_to root_url
    end
  end

  # has_one löscht alten Job oder Parent automatisch bei .new, aber ID darf sich nicht verändern, daher: no_profile_reset
  def no_profile_reset
    if (parent? or job?)
      flash[:info] = "Sie haben bereits ein Profil erstellt. Über ihr Profilfeld können sie dieses anpassen."
      redirect_to current_user
    end
  end

  def is_premium
    unless (parent? and current_user.premium?)
      flash[:info] = "Diese Funktion steht nur Eltern mit Premiumaccount zur Verfügung"
      redirect_to current_user
    end
  end

  def is_the_admin
    if (logged_in? and current_user.admin? )
      flash[:info] = "Sie sind der Admin."
      redirect_to current_user
    end
  end

  def only_admins
    unless current_user.admin?
      flash[:info] = "Ups, da fehlt die Berechtigung."
      redirect_to current_user
    end
  end

  def only_parents
    if job?
      flash[:info] = "Sie sind bereits angemeldet."
      redirect_to current_user
    end
  end
  def only_nannies
    if parent?
      flash[:info] = "Sie sind bereits angemeldet."
      redirect_to current_user
    end
  end

  def access_permission_ratings
    # sichtbar wenn: current_user? oder admin? oder parent mit premium account und gematcht mit @user? oder job und von @user eine Anfrage bekommen? oder auf Merkliste wenn parent?-->
    @user = User.find(params[:id])
    unless
    logged_in? and (current_user?(@user) or current_user.admin? or
        (parent? and current_user.premium? and !current_user.parent.sorting.nil? and
            current_user.parent.sorting.helpfield.split(",").include?(@user.id.to_s)) or
        (parent? and current_user.premium? and !@user.job.nil? and !current_user.parent.active_requests.nil? and
            !current_user.parent.active_requests.where("(paid = 't')").nil? and
            current_user.parent.active_requests.where("(paid = 't')").pluck(:potential_id).include?(@user.job.id)) or
        (job? and !current_user.job.passive_requests.nil? and !@user.parent.nil? and
            !current_user.job.passive_requests.find_by(interested_id: @user.parent.id).nil?) or
        (!current_user.active_relationships.nil? and
            !current_user.active_relationships.find_by(followed_id: @user.id).nil?))

      flash[:info] = "Diese Funktion steht Ihnen leider nicht zur Verfügung."
      if !current_user.nil?
        redirect_to current_user
      else
        redirect_to root_url
      end
    end
  end

  def access_permission_profiles
    # sichtbar wenn: current_user? oder admin? oder parent und gematcht mit @user? oder parent mit standardaccount und matched mit @user oder job und von @user eine Anfrage bekommen? oder auf Merkliste (wenn premium)?-->
    @user = User.find(params[:id])
    unless
    logged_in? and (current_user?(@user) or current_user.admin? or
        (parent? and current_user.premium? and !current_user.parent.sorting.nil? and
            current_user.parent.sorting.helpfield.split(",").include?(@user.id.to_s)) or
        (parent? and (current_user.parent.assigned.to_i == @user.id)) or
        (parent? and !@user.job.nil? and !current_user.parent.active_requests.nil? and
            !current_user.parent.active_requests.find_by(potential_id: @user.job.id).nil?) or
        (job? and !current_user.job.passive_requests.nil? and !@user.parent.nil? and
            !current_user.job.passive_requests.find_by(interested_id: @user.parent.id).nil?) or
        (!current_user.active_relationships.nil? and
            !current_user.active_relationships.find_by(followed_id: @user.id).nil?))

      flash[:info] = "Diese Funktion steht Ihnen leider nicht zur Verfügung."
      if !current_user.nil?
        redirect_to current_user
      else
        redirect_to root_url
      end
    end
  end


  def Jobfind(user)
    Job.find_by(user_id: user.to_i)
  end
end



