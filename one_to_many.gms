set

m        Pr�ferenzkriterien /m0, m1*m15/
n        Nanny

;
parameter

c(m)     Pr�ferenzwerte
d(n,m)   Nannykriterien
x        Anzahl zuzuweisener Nannies;

;
free variable

ZFW

binary variable

z(n)     1 wenn Nanny n ausgew�hlt wird

;
equations

Zielfunktion
eins
;

Zielfunktion..
  sum((n,m), c(m)*d(n,m)*z(n)) =e= ZFW;

eins..
  sum(n, z(n)) =e= x;



*****
$include Nanny.inc
$include Preferences.inc
$include NannyAttributes.inc
$include searchsize.inc
*****

model matching /all/;
solve matching using mip maximizing ZFW;
display n, z.l;


parameter
p(n);
p(n) = sum(m, c(m)*d(n,m))*z.l(n);
display p;
integer variable isFirstElement;
isFirstElement.l=0;
file outputfile /'matching_results.txt'/;
put outputfile;
isFirstElement.l=0
loop(n,
 if(p(n) > 0,
   if(isFirstElement.l=0,
     isFirstElement.l=1;
     put n.tl, p(n);
   else
     put ",";
     put n.tl, p(n);
   );
  );
);
putclose outputfile;
