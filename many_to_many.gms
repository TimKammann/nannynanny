set

p        Parents
m        Pr�ferenzkriterien /m0, m1*m15/
n        Nannies

ok(p,n)

;
parameter

c(p,m)   Pr�ferenzwerte der Parents
d(n,m)   Nannykriterien

;
free variable

ZFW

binary variable

z(p,n)     1 wenn Parent p und Nanny n ausgew�hlt werden

;
*****
$include match_Nannies.inc
$include match_Parents.inc
$include match_Nannies_Attributes.inc
$include match_Parents_Preferences.inc
$include match_Filter_Results.inc
*****

;
equations

Zielfunktion
eins
zwei
;

Zielfunktion..
  sum((p,n,m)$ok(p,n), c(p,m)*d(n,m)*z(p,n)) =e= ZFW;

eins(p)..
  sum(n$ok(p,n), z(p,n)) =l= 1;

zwei(n)..
  sum(p$ok(p,n), z(p,n)) =l= 1;

model matching /all/;
matching.optcr = 0.0;
solve matching using mip maximizing ZFW;
display n,p, z.l;

parameter x(p,n);
  x(p,n) = sum(m, c(p,m)*d(n,m))*z.l(p,n);
display x;

file outputfile / 'many_to_many_results.txt'/;
put outputfile;
loop(p,
  loop(n,
   if( z.l(p,n) = 1,
    put p.tl:0 ',' n.tl /
   );
  );
);
putclose outputfile;
