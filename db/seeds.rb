srand Time.now.nsec


#---: Admin
User.create!(name:  "Admin",
             email: "stefan.helber@admin.de",
             password:              "geheim",
             password_confirmation: "geheim",
             admin: true)

#---: Users
199.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  User.create!(name:  name,
               email: email,
               password:              "asdasd",
               password_confirmation: "asdasd")
end



#---: NANNIES
# randomize all attributes, with two third probability on booleans => true

early_users = User.order(:created_at)[1..40]
early_users.each do |early_user|
  content = Faker::Lorem.sentence(25)
  params = {content: content, city: "Hannover", age: rand(15..40),
            nanny_gender: rand(1..2), gender: (rand < 0.8 ? 3 : rand(1..2)),
            kids: rand(1..4), baby: (rand < 0.75 ? true : false),
            toddler: (rand < 0.75 ? true : false), preschooler: (rand < 0.75 ? true : false),
            gradeschooler: (rand < 0.75 ? true : false), teenager: (rand < 0.75 ? true : false),
            experience: rand(1..4), rate: rand(9..30), nonsmoker: (rand < 0.75 ? true : false),
            first_aid: (rand < 0.75 ? true : false), babysitter_certificate: (rand < 0.75 ? true : false),
            pets: (rand < 0.75 ? true : false), licence: (rand < 0.75 ? true : false),
            car: (rand < 0.75 ? true : false), laundry: (rand < 0.75 ? true : false),
            run_errands: (rand < 0.75 ? true : false), to_bed: (rand < 0.75 ? true : false),
            cooking: (rand < 0.75 ? true : false), bodycare: (rand < 0.75 ? true : false),
            feed: (rand < 0.75 ? true : false), read: (rand < 0.75 ? true : false),
            music: (rand < 0.75 ? true : false), sports: (rand < 0.75 ? true : false),
            swimming: (rand < 0.75 ? true : false), painting: (rand < 0.75 ? true : false),
            excursions: (rand < 0.75 ? true : false), homework: (rand < 0.75 ? true : false),
            valid_from: "2017-03-21", valid_until: "2017-05-31", time: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19"}
  begin
    job = Job.create(params)
    early_user.job = job
  rescue => e
  end
end

late_users = User.order(:created_at)[41..80]
late_users.each do |late_user|
  content = Faker::Lorem.sentence(25)
  params = {content: content, city: "Hannover", age: rand(15..40),
            nanny_gender: rand(1..2), gender: (rand < 0.8 ? 3 : rand(1..2)),
            kids: rand(1..4), baby: (rand < 0.75 ? true : false),
            toddler: (rand < 0.75 ? true : false), preschooler: (rand < 0.75 ? true : false),
            gradeschooler: (rand < 0.75 ? true : false), teenager: (rand < 0.75 ? true : false),
            experience: rand(1..4), rate: rand(9..30), nonsmoker: (rand < 0.75 ? true : false),
            first_aid: (rand < 0.75 ? true : false), babysitter_certificate: (rand < 0.75 ? true : false),
            pets: (rand < 0.75 ? true : false), licence: (rand < 0.75 ? true : false),
            car: (rand < 0.75 ? true : false), laundry: (rand < 0.75 ? true : false),
            run_errands: (rand < 0.75 ? true : false), to_bed: (rand < 0.75 ? true : false),
            cooking: (rand < 0.75 ? true : false), bodycare: (rand < 0.75 ? true : false),
            feed: (rand < 0.75 ? true : false), read: (rand < 0.75 ? true : false),
            music: (rand < 0.75 ? true : false), sports: (rand < 0.75 ? true : false),
            swimming: (rand < 0.75 ? true : false), painting: (rand < 0.75 ? true : false),
            excursions: (rand < 0.75 ? true : false), homework: (rand < 0.75 ? true : false),
            valid_from: "2017-03-21", valid_until: "2017-05-31", time: "15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35"}
  begin
    job = Job.create(params)
    late_user.job = job
  rescue => e
  end
end

restless_users = User.order(:created_at)[81..100]
restless_users.each do |restless_user|
  content = Faker::Lorem.sentence(25)
  params = {content: content, city: "Hannover", age: rand(15..40),
            nanny_gender: rand(1..2), gender: (rand < 0.8 ? 3 : rand(1..2)),
            kids: rand(1..4), baby: (rand < 0.75 ? true : false),
            toddler: (rand < 0.75 ? true : false), preschooler: (rand < 0.75 ? true : false),
            gradeschooler: (rand < 0.75 ? true : false), teenager: (rand < 0.75 ? true : false),
            experience: rand(1..4), rate: rand(9..30), nonsmoker: (rand < 0.75 ? true : false),
            first_aid: (rand < 0.75 ? true : false), babysitter_certificate: (rand < 0.75 ? true : false),
            pets: (rand < 0.75 ? true : false), licence: (rand < 0.75 ? true : false),
            car: (rand < 0.75 ? true : false), laundry: (rand < 0.75 ? true : false),
            run_errands: (rand < 0.75 ? true : false), to_bed: (rand < 0.75 ? true : false),
            cooking: (rand < 0.75 ? true : false), bodycare: (rand < 0.75 ? true : false),
            feed: (rand < 0.75 ? true : false), read: (rand < 0.75 ? true : false),
            music: (rand < 0.75 ? true : false), sports: (rand < 0.75 ? true : false),
            swimming: (rand < 0.75 ? true : false), painting: (rand < 0.75 ? true : false),
            excursions: (rand < 0.75 ? true : false), homework: (rand < 0.75 ? true : false),
            valid_from: "2017-03-21", valid_until: "2017-05-31", time: "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35"}
  begin
    job = Job.create(params)
    restless_user.job = job
  rescue => e
  end
end



#---: PARENTS
early_parents = User.order(:created_at)[101..150]
early_parents.each do |early_parent|
  content = Faker::Lorem.sentence(25)

  # sicherstellen, dass ein eintrag wie 2 kids => baby,toddler,teenager nicht möglich ist
  kids = rand(1..4)
  kidsCount = 0
  if rand < 0.5
    kidsCount += 1
    baby = true
  else
    baby = false
  end
  if (((kids == 4) or (kidsCount < kids)) and (rand < 0.5))
    kidsCount += 1
    preschooler = true
  else
    preschooler = false
  end
  if (((kids == 4) or (kidsCount < kids)) and (rand < 0.5))
    kidsCount += 1
    gradeschooler = true
  else
    gradeschooler = false
  end
  if (((kids == 4) or (kidsCount < kids)) and (rand < 0.5))
    kidsCount += 1
    teenager = true
  else
    teenager = false
  end
  if (((kids == 4) or (kidsCount < kids)) and (rand < 0.5))
    kidsCount += 1
    toddler = true
  else
    toddler = false
  end
  if (kidsCount == 0)
    toddler = true
  end
  # 20% wahrscheinlichkeit für filterattribute
  params = {content: content, city: "Hannover", lower_age: rand(15..22), upper_age: rand(28..40),
            nanny_gender: rand(1..3), gender: rand(1..3),
            kids: kids, baby: baby,
            toddler: toddler, preschooler: preschooler,
            gradeschooler: gradeschooler, teenager: teenager,
            experience: rand(1..4), lower_rate: rand(9..12), upper_rate: rand(18..30),
            nonsmoker: (rand < 0.5 ? true : false), pets: (rand < 0.5 ? true : false),
            first_aid: (rand < 0.8 ? (rand*2).round(1) : 3), babysitter_certificate: (rand < 0.8 ? (rand*2).round(1) : 3),
            licence: (rand < 0.8 ? (rand*2).round(1) : 3), car: (rand < 0.8 ? (rand*2).round(1) : 3),
            laundry: (rand < 0.8 ? (rand*2).round(1) : 3), run_errands: (rand < 0.8 ? (rand*2).round(1) : 3),
            to_bed: (rand < 0.8 ? (rand*2).round(1) : 3), cooking: (rand < 0.8 ? (rand*2).round(1) : 3),
            bodycare: (rand < 0.8 ? (rand*2).round(1) : 3), feed: (rand < 0.8 ? (rand*2).round(1) : 3),
            read: (rand < 0.8 ? (rand*2).round(1) : 3), music: (rand < 0.8 ? (rand*2).round(1) : 3),
            sports: (rand < 0.8 ? (rand*2).round(1) : 3), swimming: (rand < 0.8 ? (rand*2).round(1) : 3),
            painting: (rand < 0.8 ? (rand*2).round(1) : 3), excursions: (rand < 0.8 ? (rand*2).round(1) : 3),
            homework:(rand < 0.8 ? (rand*2).round(1) : 3), valid_from: "2017-04-07", valid_until: "2017-04-14",
            time: "1,2,3,4,5,6,7,8,9,10,11,12,13,14"}
  begin
    parent = Parent.create(params)
    early_parent.parent = parent
  rescue => e
  end
end


late_parents = User.order(:created_at)[151..199]
late_parents.each do |late_parent|
  content = Faker::Lorem.sentence(25)

  # sicherstellen, dass ein eintrag wie 2 kids => baby,toddler,tennager nicht möglich ist
  # höhere wahrscheinlichkeit für jüngere Kinder
  kids = rand(1..4)
  kidsCount = 0
  if rand < 0.5
    kidsCount += 1
    baby = true
  else
    baby = false
  end
  if (((kids == 4) or (kidsCount < kids)) and (rand < 0.5))
    kidsCount += 1
    preschooler = true
  else
    preschooler = false
  end
  if (((kids == 4) or (kidsCount < kids)) and (rand < 0.5))
    kidsCount += 1
    gradeschooler = true
  else
    gradeschooler = false
  end
  if (((kids == 4) or (kidsCount < kids)) and (rand < 0.5))
    kidsCount += 1
    teenager = true
  else
    teenager = false
  end
  if (((kids == 4) or (kidsCount < kids)) and (rand < 0.5))
    kidsCount += 1
    toddler = true
  else
    toddler = false
  end
  if (kidsCount == 0)
    toddler = true
  end
  # 20% wahrscheinlichkeit für filterattribute
  params = {content: content, city: "Hannover", lower_age: rand(15..22), upper_age: rand(28..40),
            nanny_gender: rand(1..3), gender: rand(1..3),
            kids: kids, baby: baby,
            toddler: toddler, preschooler: preschooler,
            gradeschooler: gradeschooler, teenager: teenager,
            experience: rand(1..4), lower_rate: rand(9..12), upper_rate: rand(18..30),
            nonsmoker: (rand < 0.5 ? true : false), pets: (rand < 0.5 ? true : false),
            first_aid: (rand < 0.8 ? (rand*2).round(1) : 3), babysitter_certificate: (rand < 0.8 ? (rand*2).round(1) : 3),
            licence: (rand < 0.8 ? (rand*2).round(1) : 3), car: (rand < 0.8 ? (rand*2).round(1) : 3),
            laundry: (rand < 0.8 ? (rand*2).round(1) : 3), run_errands: (rand < 0.8 ? (rand*2).round(1) : 3),
            to_bed: (rand < 0.8 ? (rand*2).round(1) : 3), cooking: (rand < 0.8 ? (rand*2).round(1) : 3),
            bodycare: (rand < 0.8 ? (rand*2).round(1) : 3), feed: (rand < 0.8 ? (rand*2).round(1) : 3),
            read: (rand < 0.8 ? (rand*2).round(1) : 3), music: (rand < 0.8 ? (rand*2).round(1) : 3),
            sports: (rand < 0.8 ? (rand*2).round(1) : 3), swimming: (rand < 0.8 ? (rand*2).round(1) : 3),
            painting: (rand < 0.8 ? (rand*2).round(1) : 3), excursions: (rand < 0.8 ? (rand*2).round(1) : 3),
            homework:(rand < 0.8 ? (rand*2).round(1) : 3), valid_from: "2017-04-07", valid_until: "2017-04-21",
            time: "15,16,20,21,22,23,24,25,26,27,28"}
  begin
    parent = Parent.create(params)
    late_parent.parent = parent
  rescue => e
  end
end


#--: Bewertungen
rating_nannies = User.order(:created_at)[1..5]
rated_parents = User.order(:created_at)[101..105]
rated_parents.each do |rated_parent|
  rating_nannies.each do |rating_nanny|
  content = Faker::Lorem.sentence(25)
  params = {receiver_id: rated_parent.id, sender_id: rating_nanny.id, content: content, parent_tidiness: rand(1..5), parent_kindness: rand(1..5),
            parent_correctness: rand(1..5), parent_recommendation: rand(1..5)}
  begin
    Rating.create(params)
  rescue => e
  end
  end
end
rating_parents = User.order(:created_at)[101..105]
rated_nannies = User.order(:created_at)[1..5]
rated_nannies.each do |rated_nanny|
  rating_parents.each do |rating_parent|
    content = Faker::Lorem.sentence(25)
    params = {receiver_id: rated_nanny.id, sender_id: rating_parent.id, content: content, job_punctuality: rand(1..5), job_satisfaction_of_child: rand(1..5),
              job_correctness: rand(1..5), job_recommendation: rand(1..5)}
    begin
      Rating.create(params)
    rescue => e
    end
  end
end


