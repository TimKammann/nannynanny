# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170320112420) do

  create_table "calculators", force: :cascade do |t|
    t.string   "gams"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "calculators", ["user_id", "created_at"], name: "index_calculators_on_user_id_and_created_at"
  add_index "calculators", ["user_id"], name: "index_calculators_on_user_id"

  create_table "conversations", force: :cascade do |t|
    t.integer  "parent_user_id"
    t.integer  "job_user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "conversations", ["job_user_id"], name: "index_conversations_on_job_user_id"
  add_index "conversations", ["parent_user_id"], name: "index_conversations_on_parent_user_id"

  create_table "jobs", force: :cascade do |t|
    t.text     "content"
    t.boolean  "nonsmoker"
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.boolean  "homework"
    t.string   "valid_from"
    t.string   "valid_until"
    t.string   "time"
    t.string   "city"
    t.integer  "age"
    t.integer  "nanny_gender"
    t.integer  "gender"
    t.integer  "kids"
    t.boolean  "baby"
    t.boolean  "toddler"
    t.boolean  "preschooler"
    t.boolean  "gradeschooler"
    t.boolean  "teenager"
    t.integer  "experience"
    t.integer  "rate"
    t.boolean  "first_aid"
    t.boolean  "babysitter_certificate"
    t.boolean  "pets"
    t.boolean  "licence"
    t.boolean  "car"
    t.boolean  "laundry"
    t.boolean  "run_errands"
    t.boolean  "to_bed"
    t.boolean  "cooking"
    t.boolean  "bodycare"
    t.boolean  "feed"
    t.boolean  "read"
    t.boolean  "music"
    t.boolean  "sports"
    t.boolean  "swimming"
    t.boolean  "painting"
    t.boolean  "excursions"
  end

  add_index "jobs", ["time"], name: "index_jobs_on_time"
  add_index "jobs", ["user_id", "created_at"], name: "index_jobs_on_user_id_and_created_at"
  add_index "jobs", ["user_id"], name: "index_jobs_on_user_id"
  add_index "jobs", ["valid_from", "valid_until"], name: "index_jobs_on_valid_from_and_valid_until"

  create_table "messages", force: :cascade do |t|
    t.integer  "conversation_id"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id"
  add_index "messages", ["user_id"], name: "index_messages_on_user_id"

  create_table "parents", force: :cascade do |t|
    t.text     "content"
    t.boolean  "nonsmoker"
    t.float    "homework"
    t.text     "assigned",               default: "0"
    t.integer  "user_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "valid_from"
    t.string   "valid_until"
    t.string   "time"
    t.string   "city"
    t.string   "address"
    t.integer  "lower_age"
    t.integer  "upper_age"
    t.integer  "gender"
    t.integer  "kids"
    t.boolean  "baby"
    t.boolean  "toddler"
    t.boolean  "preschooler"
    t.boolean  "gradeschooler"
    t.boolean  "teenager"
    t.integer  "experience"
    t.integer  "lower_rate"
    t.integer  "upper_rate"
    t.float    "first_aid"
    t.float    "babysitter_certificate"
    t.boolean  "pets"
    t.float    "licence"
    t.float    "car"
    t.float    "laundry"
    t.float    "run_errands"
    t.float    "to_bed"
    t.float    "cooking"
    t.float    "bodycare"
    t.float    "feed"
    t.float    "read"
    t.float    "music"
    t.float    "sports"
    t.float    "swimming"
    t.float    "painting"
    t.float    "excursions"
    t.integer  "nanny_gender"
  end

  add_index "parents", ["time"], name: "index_parents_on_time"
  add_index "parents", ["user_id", "created_at"], name: "index_parents_on_user_id_and_created_at"
  add_index "parents", ["user_id"], name: "index_parents_on_user_id"
  add_index "parents", ["valid_from", "valid_until"], name: "index_parents_on_valid_from_and_valid_until"

  create_table "ratings", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.text     "content"
    t.integer  "parent_tidiness"
    t.integer  "parent_kindness"
    t.integer  "parent_recommendation"
    t.integer  "parent_correctness"
    t.integer  "job_punctuality"
    t.integer  "job_satisfaction_of_child"
    t.integer  "job_correctness"
    t.integer  "job_recommendation"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "ratings", ["receiver_id"], name: "index_ratings_on_receiver_id"
  add_index "ratings", ["sender_id", "receiver_id"], name: "index_ratings_on_sender_id_and_receiver_id"
  add_index "ratings", ["sender_id"], name: "index_ratings_on_sender_id"

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "relationships", ["followed_id"], name: "index_relationships_on_followed_id"
  add_index "relationships", ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
  add_index "relationships", ["follower_id"], name: "index_relationships_on_follower_id"

  create_table "requests", force: :cascade do |t|
    t.integer  "interested_id"
    t.integer  "potential_id"
    t.boolean  "paid",          default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "accepted",      default: false
    t.string   "start_date"
    t.string   "end_date"
    t.string   "time"
  end

  add_index "requests", ["interested_id"], name: "index_requests_on_interested_id"
  add_index "requests", ["potential_id"], name: "index_requests_on_potential_id"
  add_index "requests", ["start_date", "end_date"], name: "index_requests_on_start_date_and_end_date"
  add_index "requests", ["time"], name: "index_requests_on_time"

  create_table "sortings", force: :cascade do |t|
    t.boolean  "by_matching"
    t.boolean  "by_rate"
    t.boolean  "by_created_at"
    t.text     "helpfield"
    t.text     "helpfield2"
    t.integer  "parent_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "sortings", ["parent_id"], name: "index_sortings_on_parent_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",           default: false
    t.boolean  "premium"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
