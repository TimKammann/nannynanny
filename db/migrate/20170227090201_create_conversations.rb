class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.integer :parent_user_id
      t.integer :job_user_id

      t.timestamps null: false
    end
    add_index :conversations, :parent_user_id
    add_index :conversations, :job_user_id
    # add_index :conversations, [:parent_user_id, :job_user_id, unique: true]
  end
end
