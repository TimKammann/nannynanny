class AddTimesToRequests < ActiveRecord::Migration
  def change
    add_column :requests, :start_date, :string
    add_column :requests, :end_date, :string
    add_column :requests, :time, :string

    add_index :requests, [:start_date, :end_date]
    add_index :requests, :time
  end
end
