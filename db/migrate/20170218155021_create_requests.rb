class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.integer :interested_id
      t.integer :potential_id
      t.boolean :paid, default: false

      t.timestamps null: false
    end
    add_index :requests, :interested_id
    add_index :requests, :potential_id
    #no unique constraint -> nach abgeschlossenen Job nochmal buchen
  end
end
