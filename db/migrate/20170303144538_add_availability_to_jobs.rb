class AddAvailabilityToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :valid_from, :string
    add_column :jobs, :valid_until, :string
    add_column :jobs, :time, :string

    add_index :jobs, [:valid_from, :valid_until]
    add_index :jobs, :time
  end
end
