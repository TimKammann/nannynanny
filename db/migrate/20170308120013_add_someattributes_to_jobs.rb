class AddSomeattributesToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :city, :string
    add_column :jobs, :age, :integer
    add_column :jobs, :nanny_gender, :integer
    add_column :jobs, :gender, :integer
    add_column :jobs, :kids, :integer
    add_column :jobs, :baby, :boolean
    add_column :jobs, :toddler, :boolean
    add_column :jobs, :preschooler, :boolean
    add_column :jobs, :gradeschooler, :boolean
    add_column :jobs, :teenager, :boolean
    add_column :jobs, :experience, :integer
    add_column :jobs, :rate, :integer
    add_column :jobs, :first_aid, :boolean
    add_column :jobs, :babysitter_certificate, :boolean
    add_column :jobs, :pets, :boolean
    add_column :jobs, :licence, :boolean
    add_column :jobs, :car, :boolean
    add_column :jobs, :laundry, :boolean
    add_column :jobs, :run_errands, :boolean
    add_column :jobs, :to_bed, :boolean
    add_column :jobs, :cooking, :boolean
    add_column :jobs, :bodycare, :boolean
    add_column :jobs, :feed, :boolean
    add_column :jobs, :read, :boolean
    add_column :jobs, :music, :boolean
    add_column :jobs, :sports, :boolean
    add_column :jobs, :swimming, :boolean
    add_column :jobs, :painting, :boolean
    add_column :jobs, :excursions, :boolean
  end
end
