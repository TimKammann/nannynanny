class AddHomeworkToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :homework, :boolean
  end
end
