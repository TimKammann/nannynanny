class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email

      t.timestamps null: false
    end
  end
end


# create_table is a Rails method to create a table in the database for storing users,
# it accepts a block with a block variable, here the block is called t for table
