class CreateSortings < ActiveRecord::Migration
  def change
    create_table :sortings do |t|
      t.boolean :by_matching
      t.boolean :by_rate
      t.boolean :by_created_at
      t.text :helpfield
      t.text :helpfield2
      t.references :parent, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
