class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :sender_id
      t.integer :receiver_id
      t.text :content
      t.integer :parent_tidiness
      t.integer :parent_kindness
      t.integer :parent_recommendation
      t.integer :parent_correctness
      t.integer :job_punctuality
      t.integer :job_satisfaction_of_child
      t.integer :job_correctness
      t.integer :job_recommendation

      t.timestamps null: false
    end
    add_index :ratings, :sender_id
    add_index :ratings, :receiver_id
    add_index :ratings, [:sender_id, :receiver_id]

  end
end
