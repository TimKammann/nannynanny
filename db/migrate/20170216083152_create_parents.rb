class CreateParents < ActiveRecord::Migration
  def change
    create_table :parents do |t|
      t.text :content
      t.boolean :nonsmoker
      t.float :homework
      t.text :assigned, default: 0
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :parents, [:user_id, :created_at]
  end
end
