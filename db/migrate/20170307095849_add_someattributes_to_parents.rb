class AddSomeattributesToParents < ActiveRecord::Migration
  def change
    add_column :parents, :city, :string
    add_column :parents, :address, :string
    add_column :parents, :lower_age, :integer
    add_column :parents, :upper_age, :integer
    add_column :parents, :gender, :integer
    add_column :parents, :kids, :integer
    add_column :parents, :baby, :boolean
    add_column :parents, :toddler, :boolean
    add_column :parents, :preschooler, :boolean
    add_column :parents, :gradeschooler, :boolean
    add_column :parents, :teenager, :boolean
    add_column :parents, :experience, :integer
    add_column :parents, :lower_rate, :integer
    add_column :parents, :upper_rate, :integer
    add_column :parents, :first_aid, :float
    add_column :parents, :babysitter_certificate, :float
    add_column :parents, :pets, :boolean
    add_column :parents, :licence, :float
    add_column :parents, :car, :float
    add_column :parents, :laundry, :float
    add_column :parents, :run_errands, :float
    add_column :parents, :to_bed, :float
    add_column :parents, :cooking, :float
    add_column :parents, :bodycare, :float
    add_column :parents, :feed, :float
    add_column :parents, :read, :float
    add_column :parents, :music, :float
    add_column :parents, :sports, :float
    add_column :parents, :swimming, :float
    add_column :parents, :painting, :float
    add_column :parents, :excursions, :float

  end
end
