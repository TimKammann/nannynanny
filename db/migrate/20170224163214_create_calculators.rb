class CreateCalculators < ActiveRecord::Migration
  def change
    create_table :calculators do |t|
      t.string :gams
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :calculators, [:user_id, :created_at]
  end
end
