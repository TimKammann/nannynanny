class AddTimeRequestToParents < ActiveRecord::Migration
  def change
    add_column :parents, :valid_from, :string
    add_column :parents, :valid_until, :string
    add_column :parents, :time, :string

    add_index :parents, [:valid_from, :valid_until]
    add_index :parents, :time
  end
end
