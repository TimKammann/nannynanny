class AddNannyGenderToParents < ActiveRecord::Migration
  def change
    add_column :parents, :nanny_gender, :integer
  end
end
