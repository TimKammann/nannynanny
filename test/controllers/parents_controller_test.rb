require 'test_helper'

class ParentsControllerTest < ActionController::TestCase

    def setup
      @user = users(:one)
      @parent = parents(:valid)
      @user.parent = @parent
    end

    test "require gams for one-to-many matching" do
      log_in_as(@parent.user)
      get :search
      assert_redirected_to calculators_path
    end

end
