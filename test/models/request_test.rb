class RequestTest < ActiveSupport::TestCase


  test 'requests is valid between an interested parent and potential job' do
    request = Request.new(interested_id: 1, potential_id: 2)
    assert request.valid?
  end

  test 'invalid request when no potential job' do
    request = Request.new(interested_id: 1)
    refute request.valid?, 'date fields are the problem'
  end

end