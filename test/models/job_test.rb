require 'test_helper'

class JobTest < ActiveSupport::TestCase

  def setup
    @job = jobs(:valid)
  end

  test 'valid job' do
    assert @job.valid?
  end

  test 'invalid without requested calender times' do
    user = Job.new(user_id: 2, city: "Hannover", content: "bambla", valid_from: "2017-03-24",
                   valid_until: "2017-04-01", time: "", kids: 1, nanny_gender: 3, gender: 1,
                   age: 15, rate: 12, experience: 1, baby: true)
    refute user.valid?, 'time field is the problem'
  end

  test 'invalid without requested calender dates' do
    user = Job.new(user_id: 2, city: "Hannover", content: "bambla", valid_from: "",
                   valid_until: "", time: "1,2,3,4,5,6,7", kids: 1, nanny_gender: 3, gender: 1,
                   age: 15, rate: 12, experience: 1, baby: true)
    refute user.valid?, 'date fields are the problem'
  end

end