require 'test_helper'

class ParentTest < ActiveSupport::TestCase

  def setup
    @parent = parents(:valid)
  end

  test 'valid parent' do
    assert @parent.valid?
  end

  test 'invalid without requested calender times' do
    user = Parent.new(user_id: 1, city: "Hannover", content: "jolifanto", valid_from: "2017-03-24",
                      valid_until: "2017-04-01", time: "", kids: 1, nanny_gender: 3, gender: 1,
                      lower_age: 15, upper_age: 29, experience: 1, baby: true)
    refute user.valid?, 'time field is the problem'
  end

  test 'invalid without requested calender dates' do
    user = Parent.new(user_id: 1, city: "Hannover", content: "jolifanto", valid_from: "",
                      valid_until: "", time: "1,2,3,4,5,6,7", kids: 1, nanny_gender: 3, gender: 1,
                      lower_age: 15, upper_age: 29, experience: 1, baby: true)
    refute user.valid?, 'date fields are the problem'
  end

end
