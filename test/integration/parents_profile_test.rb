require 'test_helper'

class ParentsProfileTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:one)
    @parent = parents(:valid)
  end

  test "redirect when user already has parent profile" do
    @user.parent = @parent
    log_in_as(@user)
    get new_parent_path
    assert_redirected_to @user
    assert_not flash.empty?
  end
end

