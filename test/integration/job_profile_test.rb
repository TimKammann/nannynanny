require 'test_helper'

class JobProfileTest < ActionDispatch::IntegrationTest
  require 'test_helper'


  def setup
    @user = users(:one)
    @job = jobs(:valid)
  end

  test "redirect when user already has parent profile" do
    @user.job = @job
    log_in_as(@job.user)
    get new_job_path
    assert_redirected_to @user
    assert_not flash.empty?
  end
end
