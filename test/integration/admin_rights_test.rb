require 'test_helper'

class AdminRightsTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:one)
    @admin = users(:admin)
  end

  test "get index view only when admin" do
    log_in_as(@user)
    get parents_path
    assert_redirected_to @user
    get jobs_path
    assert_redirected_to @user

    log_in_as(@admin)
    get parents_path
    assert_template 'parents/index'
    get jobs_path
    assert_template 'jobs/index'
  end

  test "require GAMS path for matching" do
    log_in_as(@admin)
    post matchings_match_path
    assert_redirected_to calculators_path
  end
end
